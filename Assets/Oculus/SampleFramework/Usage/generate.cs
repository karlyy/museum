using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using UnityEngine;
using UnityEngine.Networking;

public class generate : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        //string book = "https://drive.google.com/file/d/1hWQ8T-9VdBYSRLGBj0fHyBGMBqNVjyh4/view?usp=sharing";
        string book = "Assets/Oculus/SampleFramework/Usage/test.txt";

        //Debug.Log(book);
        //string url = "https://docs.unity3d.com/uploads/Main/ShadowIntro.png";

        List<string> urls = Read(book);
        for(int i=0; i < 12 && i<urls.Count; i++)
        {
            //Debug.Log(i+"    "+ urls[i]);
            AddPoint(urls, i);
        }

        //StartCoroutine(GetTexture(url, paint[i]));
    }

    private List<string> Read(string book)
    {
        List<string> urls = new List<string>();
        //Debug.Log(000);
        using (WebClient client = new WebClient())
        {
            //Debug.Log(111);
            using (Stream stream = client.OpenRead(book))
            {
                //Debug.Log(222);
                using (StreamReader reader = new StreamReader(stream))
                {
                    string line;

                    while ((line = reader.ReadLine()) != null)
                    {
                        urls.Add(line);
                    }
                }
            }
        }
         return urls;
    }

    private void AddPoint(List<string> urls, int i)
    {
        GameObject paint = GameObject.CreatePrimitive(PrimitiveType.Plane);
        Renderer rend = paint.GetComponent<Renderer>();
        paint.GetComponent<Renderer>().material = new Material(Shader.Find("Standard"));
        //Debug.Log(urls[i]);
        StartCoroutine(GetTexture(urls[i], paint));
        if(i%2!=0)
            paint.transform.SetPositionAndRotation(new Vector3((float)(10 * (i-1)), 2, 0), new Quaternion(90, 0, 0, 90));

         /*   paint.transform.SetPositionAndRotation(new Vector3((float)(10*i), 2, 10),new Quaternion(-90, 0, 180, 90));
        else
            paint.transform.SetPositionAndRotation(new Vector3((float)(10 * (i-1)), 2, 0), new Quaternion(90, 0, 0, 90));*/
    }

    IEnumerator GetTexture(string url, GameObject go)
    {
        UnityWebRequest www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log(www.error);
        }
        else
        {
            //Texture myTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
            go.GetComponent<Renderer>().material.mainTexture = ((DownloadHandlerTexture)www.downloadHandler).texture;
        }
    }

}